import React, { Component } from "react";
import Header from "./Header";
import Content from "./Content";
import Navigate from "./Navigate";
import Footer from "./Footer";
export default class Layout extends Component {
  render() {
    return (
      <div>
        <Navigate />
        <Header />
        <Content />
        <Footer />
      </div>
    );
  }
}
